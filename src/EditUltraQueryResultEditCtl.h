#ifndef _H_EDITULTRA_QUERYRESULTEDITCTL_
#define _H_EDITULTRA_QUERYRESULTEDITCTL_

#include "framework.h"

int CreateQueryResultEditCtl( struct TabPage *pnodeTabPage , HFONT hFont );

int AppendSqlQueryResultEditText( HWND hWnd , char *format , ... );

#endif
