#ifndef _H_EDITULTRA_TABPAGES_
#define _H_EDITULTRA_TABPAGES_

#include "framework.h"

extern HWND	g_hwndTabPages ;
extern RECT	g_rectTabPages ;
extern HMENU	g_hTabPagePopupMenu ;
extern HMENU	g_hEditorPopupMenu ;
extern HMENU	g_hSymbolListPopupMenu ;
extern HMENU	g_hSymbolTreePopupMenu ;
extern int	g_nTabsHeight ;

extern HWND	g_hwndTabCloseButton ;
extern struct TabPage	*g_pnodeCloseButtonTabPage ;
/*
extern HBRUSH	g_brushCurrentTabCloseButton ;
*/
extern HBRUSH	g_brushTabCloseButton ;

struct TabPage
{
	HWND				hwndScintilla ;
	SciFnDirect			pfuncScintilla ;
	sptr_t				pScintilla ;
	RECT				rectScintilla ;
	BOOL				bHasTextSelected ;

	HWND				hwndSymbolList ;
	RECT				rectSymbolList ;

	pcre				*pcreSymbolRe ;

	HWND				hwndSymbolTree ;
	RECT				rectSymbolTree ;

	HWND				hwndQueryResultEdit ;
	RECT				rectQueryResultEdit ;
	HWND				hwndQueryResultTable ;
	RECT				rectQueryResultListView ;

	struct DatabaseConnectionConfig		stDatabaseConnectionConfig ;
	struct DatabaseConnectionHandles	stDatabaseConnectionHandles ;
	BOOL					bIsDatabaseConnected ;

	struct RedisConnectionConfig		stRedisConnectionConfig ;
	struct RedisConnectionHandles		stRedisConnectionHandles ;
	BOOL					bIsRedisConnected ;

	char				acPathName[ MAX_PATH ] ;
	char				acPathFilename[ MAX_PATH ] ;
	char				acFilename[ MAX_PATH ] ;
	size_t				nFilenameLen ;
	char				acExtname[ _MAX_EXT ] ;
	time_t				st_mtime ;

	char				acEndOfLine[ 2+1 ] ;
	struct DocTypeConfig		*pstDocTypeConfig ;

	struct EncodingProbe		stEncodingProbe ;
	UINT				nCodePage ;
	char				acPreFileContext[ 4 + 1 ] ;
	size_t				nPreFileContextLen ;
	BOOL				bNeedWrotePreFileContext ;

	struct NewLineProbe		stNewLineModeProbe ;

	struct RemoteFileServer		stRemoteFileServer ;

	size_t				nFileSize ;
	size_t				nWroteLen ;
	char				*acWriteFileBuffer ;

	BOOL				bHexEditMode ;
	int				nByteCountInLastLine ;
};

extern struct TabPage			*g_pnodeCurrentTabPage ;

int CreateTabPages( HWND hWnd );

void AdjustTabPages();
void AdjustTabPageBox( struct TabPage *pnodeTabPage );

struct TabPage *AddTabPage( struct RemoteFileServer *pstRemoteFileServer , char *pcPathFilename , char *pcFilename , char *pcExtname );
int RemoveTabPage( struct TabPage *pnodeTabPage );
void SetTabPageTitle( int nTabPageNo , char *title );
void SelectTabPage( struct TabPage *pnodeTabPage , int nTabPageIndex );
struct TabPage *GetTabPageByScintilla( void *hwndScintilla );
struct TabPage *GetTabPageByIndex( int nTabPageIndex );
struct TabPage *SelectTabPageByIndex( int nTabPageIndex );
void SetCurrentTabPageHighLight( int nCurrentTabPageIndex );

int OnSelectChange();
int OnSymbolListDbClick( struct TabPage *pnodeTabPage );

int CalcTabPagesHeight();

#define CONFIG_KEY_MATERIAL_TABPAGES		"TABPAG"

#endif
